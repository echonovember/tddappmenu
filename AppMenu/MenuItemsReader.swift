//
//  MenuItemsReader.swift
//  AppMenu
//
//  Created by Sergei on 13/01/17.
//  Copyright © 2017 Sergei. All rights reserved.
//

import Foundation

protocol MenuItemsReader {
    func readMenuItems() -> ([[String : String]]?, NSError?)
}

let MenuItemsPlistReaderErrorDomain = "MenuItemsPlistReaderErrorDomain"

enum MenuItemsPlistReaderErrorCode: Int {
    case FileNotFound
}

class MenuItemsPlistReader: MenuItemsReader {
    var plistToRead: String?
    
    func readMenuItems() -> ([[String : String]]?, NSError?) {
        var error: NSError? = nil
        var fileContents: [[String: String]]? = nil
        let bundle = Bundle(for: object_getClass(self))
        
        if let filePath = bundle.path(forResource: plistToRead, ofType: "plist") {
            fileContents = NSArray(contentsOfFile: filePath) as? [[String : String]]
        } else {
            error = fileNotFoundError()
        }
        
        return (fileContents, error)
    }
    
    func fileNotFoundError() -> NSError {
        let errorMessage = "\(plistToRead ?? "").plist file doesn't exist in app bundle"
        let userInfo = [NSLocalizedDescriptionKey: errorMessage]
        
        return NSError(domain: MenuItemsPlistReaderErrorDomain,
                        code: MenuItemsPlistReaderErrorCode.FileNotFound.rawValue,
                        userInfo: userInfo)
    }
}
