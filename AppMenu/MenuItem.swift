//
//  MenuItem.swift
//  AppMenu
//
//  Created by Sergei on 13/01/17.
//  Copyright © 2017 Sergei. All rights reserved.
//

import Foundation

class MenuItem {
    let title: String
    var subTitle: String?
    var iconName: String?
    
    init(title: String) {
        self.title = title
    }
}
