//
//  MenuItemsTest.swift
//  AppMenu
//
//  Created by Sergei on 13/01/17.
//  Copyright © 2017 Sergei. All rights reserved.
//

import UIKit
import XCTest

class MenuItemsTest: XCTestCase {
    var menuItem: MenuItem?
    
    override func setUp() {
        super.setUp()
        menuItem = MenuItem(title: "Contributors")
    }
    
    func testThatMenuItemHasATitle() {
        XCTAssertEqual(menuItem!.title, "Contributors", "A title should always be present")
    }
    
    func testThatMenuItemHasASubtitle() {
        menuItem!.subTitle = "Repos contributed to"
        XCTAssertEqual(menuItem!.subTitle!, "Repos contributed to", "Subtitle should be what we assigned")
    }
    
    func testThatMenuCanBeAssignedAnIconName() {
        menuItem!.iconName = "iconContributors"
        XCTAssertEqual(menuItem!.iconName!, "iconContributors", "Icon name should be what we assigned")
    }
}
